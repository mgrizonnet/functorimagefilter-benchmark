#include "otbImage.h"
#include "otbFunctorImageFilter.h"

#include "functors.h"

template <typename T, typename U>
void generic_main()
{
  //Read input image
  typedef otb::Image<T, 2> ImageType;
  typename ImageType::Pointer image;
  
  const T scale = 10.;

  using functor=Functor::MultiplyByScalar<T, U>;

  functor f;

  f.SetCoef(scale);
  
  // Create a filter that will accept an otb::Image<double> as input and produce an otb::Image<double>
  auto filter = otb::NewFunctorFilter(f);
  // Set the inputs using variadic SetVInputs
  filter->SetVInputs(image);
  // All good, lets run it
  filter->Update();

  //Image dif
  using functorDiff =Functor::ImagesDiff<T, T, U>;

  functorDiff fdiff;

  auto filterDiff = otb::NewFunctorFilter(fdiff);

  filterDiff->SetVInputs(image,image);
  filterDiff->Update();
}

template <typename V>
void generic_generic_main()
{
  generic_main<int,V>();
  generic_main<float,V>();
  generic_main<unsigned char,V>();
  generic_main<double,V>();
  generic_main<char,V>();
  generic_main<unsigned int,V>();

  generic_main<unsigned long long,V>();
  generic_main<unsigned long,V>();
  generic_main<unsigned short,V>();
  generic_main<bool,V>();
  generic_main<char,V>();
  generic_main<long double,V>();

  generic_main<char16_t,V>();
  generic_main<char32_t,V>();
  generic_main<signed char,V>();
}


int main()
{
  generic_generic_main<int>();
  generic_generic_main<float>();
  generic_generic_main<unsigned char>();
  generic_generic_main<double>();
  generic_generic_main<char>();
  generic_generic_main<unsigned int>();

  generic_generic_main<unsigned long long>();
  generic_generic_main<unsigned long>();
  generic_generic_main<unsigned short>();
  generic_generic_main<bool>();
  generic_generic_main<char>();
  generic_generic_main<long double>();

  generic_generic_main<char16_t>();
  generic_generic_main<char32_t>();
  generic_generic_main<signed char>();

  return 0;
}
