#include "otbImage.h"
#include "otbMultiplyByScalarImageFilter.h"
#include "functors.h"
#include "otbBinaryFunctorImageFilter.h"

template <typename T, typename U>
void generic_main()
{
  typedef otb::Image<T, 2> ImageType;
  typedef otb::Image<U, 2> OutputImageType;
  typename ImageType::Pointer image;
  
  // Declare the type for the Acos filter
  typedef otb::MultiplyByScalarImageFilter<ImageType, OutputImageType> FilterType;

  // Create a MultiplyScalarImage Filter
  typename FilterType::Pointer filter = FilterType::New();

  // Connect the input images
  filter->SetInput(image);

  const T scale = 10;
  filter->SetCoef(scale);

  filter->Update();
  
  //Binary Functor
  using ImagesDiffFilter = otb::BinaryFunctorImageFilter<
      ImageType, ImageType, OutputImageType,
      Functor::ImagesDiff<
          T,
          T,
        U> >;

  typename ImagesDiffFilter::Pointer filterDiff = ImagesDiffFilter::New();

  filterDiff->SetInput1(image);
  filterDiff->SetInput2(image);

  filterDiff->Update();
  
}

template <typename V>
void generic_generic_main()
{
  generic_main<int,V>();
  generic_main<float,V>();
  generic_main<unsigned char,V>();
  generic_main<double,V>();
  generic_main<char,V>();
  generic_main<unsigned int,V>();

  generic_main<unsigned long long,V>();
  generic_main<unsigned long,V>();
  generic_main<unsigned short,V>();
  generic_main<bool,V>();
  generic_main<char,V>();
  generic_main<long double,V>();

  generic_main<char16_t,V>();
  generic_main<char32_t,V>();
  generic_main<signed char,V>();
}

int main() 
{
  
  generic_generic_main<int>();
  generic_generic_main<float>();
  generic_generic_main<unsigned char>();
  generic_generic_main<double>();
  generic_generic_main<char>();
  generic_generic_main<unsigned int>();

  generic_generic_main<unsigned long long>();
  generic_generic_main<unsigned long>();
  generic_generic_main<unsigned short>();
  generic_generic_main<bool>();
  generic_generic_main<char>();
  generic_generic_main<long double>();

  generic_generic_main<char16_t>();
  generic_generic_main<char32_t>();
  generic_generic_main<signed char>();
  
  return 0;
}
