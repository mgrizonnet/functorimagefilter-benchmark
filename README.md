# Benchmark otbFunctorImageFilter

This project provides code samples with lots of templates instanciation to
compare the compilation time and the compilation RAM usage between to versions
of functor filters. One based on the current implementation in otb and itk
(UnaryFunctorImageFilter, BinaryFunctorImageFilter...) and the new
FunctorImageFilter available in MR !268 in OTB repository. 

## Prerequisites

Benchmark has been done by compiling the 2 executables against OTB branch *functor_filter*:

https://gitlab.orfeo-toolbox.org/orfeotoolbox/otb/tree/functorfilter

## Executables

- functorOldVersion.cxx : 225 instanciations of UnaryFunctorImageFilter and
  15*15 instanciation of BinaryFunctorImageFilter
  
- functorNewVersion.cxx : 225 instanciations of new FunctorFilter (with one entry) and
  15*15 instanciation of new FunctorFilter (with 2 entries)
  
NB: Note that the code compiles but will segfault as the input image is not set
in both programs.

## Results

- Configuration: On my laptop lenovo T440p SSD with 8 Go on Ubuntu 18.04
- Compiled with clang6 (tested also with gcc7)


```
time make functorOldVersion
[ 50%] Building CXX object CMakeFiles/functorOldVersion.dir/functorOldVersion.cxx.o
[100%] Linking CXX executable functorOldVersion
[100%] Built target functorOldVersion

real	0m21,291s
user	0m20,447s
sys	0m0,823s
```
```
time make functorNewVersion
[ 50%] Building CXX object CMakeFiles/functorNewVersion.dir/functorNewVersion.cxx.o
[100%] Linking CXX executable functorNewVersion
[100%] Built target functorNewVersion

real	0m13,462s
user	0m12,745s
sys	0m0,699s
```

I've seen also that the RAM usage is 10% to 30% less with the new
FunctorImageFilter implementation.

Moreover I've seen that binaries are smaller with the new version

```
28M	./functorOldVersion.dir/functorOldVersion.cxx.o
17M	./functorNewVersion.dir/functorNewVersion.cxx.o
```
