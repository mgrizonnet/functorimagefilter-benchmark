namespace Functor
{

  template<class TInput, class TOutput>
  class MultiplyByScalar
  {
  public:
    MultiplyByScalar()
    {
      m_Coef = 1.0;
    }
    virtual ~MultiplyByScalar() {}
    inline TOutput operator ()(const TInput& value) const
    {
      TOutput result;

      result = static_cast<TOutput>(m_Coef * value);

      return result;
    }

    void SetCoef(double Coef)
    {
      m_Coef = Coef;
    }
    double GetCoef(void) const
    {
      return (m_Coef);
    }

  private:
    double m_Coef;
  };


  template<class TInput1, class TInput2,  class TOutput>
  class ImagesDiff
  {
  public:
    ImagesDiff()
    {
    }
    virtual ~ImagesDiff() {}
    inline TOutput operator ()(const TInput1& value1, const TInput2& value2) const
    {
      TOutput result;

      result = static_cast<TOutput>(value1 - value2);

      return result;
    }

    constexpr size_t GetOutputSize(...) const
    {
      return 1;
    }

  };
}
